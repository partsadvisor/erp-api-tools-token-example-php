
# Token decrypt and decode example in php 
  
This project is an example to :
1. Decrypt partsadvisor JWE token
2. Check JWS signature
3. Decode JWT

This project use [web-token.spomky-labs] (https://web-token.spomky-labs.com/)

## Install PHP and composer on macos

### PHP 7.2
brew install php72
If you need to have php@7.2 first in your PATH run:
  echo 'export PATH="/usr/local/opt/php@7.2/bin:$PATH"' >> ~/.bash_profile
  echo 'export PATH="/usr/local/opt/php@7.2/sbin:$PATH"' >> ~/.bash_profile

### Install composer

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

Run composer in order to update and get dependancies

### Update project dependancies

php composer.phar update
php composer.phar install

### Run test

vendor/bin/phpunit -c phpunit.xml


