<?php


namespace com\partsadvisor;


use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128CBCHS256;
use Jose\Component\Encryption\Algorithm\KeyEncryption\Dir;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\HS256;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use PHPUnit\Framework\TestCase;

class T_decryptJWEAndDecodeJWS extends TestCase
{

    public function testDecryptJWEAndDecodeJWS(): void
    {

        function decryptJWE($inputJWEFromPartsAdvisorExample, $secretKey): string
        {

// The key encryption algorithm manager with the A256KW algorithm.
            $keyEncryptionAlgorithmManager = AlgorithmManager::create([
                new Dir(),
            ]);

// The content encryption algorithm manager with the A256CBC-HS256 algorithm.
            $contentEncryptionAlgorithmManager = AlgorithmManager::create([
                new A128CBCHS256(),
            ]);

// The compression method manager with the DEF (Deflate) method.
            $compressionMethodManager = CompressionMethodManager::create([
                new Deflate(),
            ]);

// We instantiate our JWE Decrypter.
            $jweDecrypter = new JWEDecrypter(
                $keyEncryptionAlgorithmManager,
                $contentEncryptionAlgorithmManager,
                $compressionMethodManager
            );


            $jwk = JWKFactory::createFromSecret(
                $secretKey
            );

// The JSON Converter.
            $jsonConverter = new StandardConverter();

// The serializer manager. We only use the JWE Compact Serialization Mode.
            $serializerManager = JWESerializerManager::create([
                new CompactSerializer($jsonConverter),
            ]);

// We try to load the inputJWEFromPartsAdvisorExample.
            $jwe = $serializerManager->unserialize($inputJWEFromPartsAdvisorExample);


// We decrypt the inputJWEFromPartsAdvisorExample. This method does NOT check the header.
            $result = $jweDecrypter->decryptUsingKey($jwe, $jwk, 0);

            TestCase::assertTrue($result, 'decrypt jwe error');

            $jws = $jwe->getPayload();
            echo PHP_EOL . "JWS is : " . $jws . PHP_EOL;

            // End JWE decryption
            return $jws;

        }

        function decodeJWS($jws, $signerKey): string
        {

// The algorithm manager with the HS256 algorithm.
            $algorithmManager = AlgorithmManager::create([
                new HS256(),
            ]);

// We instantiate our JWS Verifier.
            $jwsVerifier = new JWSVerifier(
                $algorithmManager
            );


// Our signer key.

            $jwk = JWKFactory::createFromSecret(
                $signerKey
            );

// The JSON Converter.
            $jsonConverter = new StandardConverter();

// The serializer manager. We only use the JWS Compact Serialization Mode.
            $serializerManager = JWSSerializerManager::create([
                new \Jose\Component\Signature\Serializer\CompactSerializer($jsonConverter),
            ]);

// We try to load the inputJWEFromPartsAdvisorExample.
            $jws = $serializerManager->unserialize($jws);

// We verify the signature. This method does NOT check the header.
// The arguments are:
// - The JWS object,
// - The key,
// - The index of the signature to check. See

            $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
            TestCase::assertTrue($isVerified, 'ERROR JWS not verify');

            TestCase::assertEquals(
                '{"sub":"5a96d12da9c216b26e9ee758","properties":{"sampleProperty":"sampleValue"}}',
                $jws->getPayload(),
                "Invalid payload in JWS"
            );

            return $jws->getPayload();
        }

        $signerKey = '[&E<J+k34AF\'Xj/G[&E<J+k34AF\'Xj/G';
        $secretKey = '67hE(c(\'@M9,w-yr67hE(c(\'@M9,w-yr';

        $inputJWEFromPartsAdvisorExample = 'eyJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..yIknbSWJh1JMTe1Pen7hRw.Zh1PAAgzr1Yx9rzLqoR3RfZzb6MgcFXPpIlOBjvJgZIjk8BcFZ6Y_E9fxNPFfLGeJXfO_yu4InY_FPqRcYfZfZ_AE2i7dXADh38P1hhzJGtGPNAm1WnzR1y_-ZErEUffMcW94TsZIrYtb9t0xXBgNoQoUsmjdiXpVnX202XdQEb__QC8xntu6-EtWl33gBxnj1H3RRZq_UxRXdCrFu_EFQJgoTOYuM-fpk-hzHqYT18.ZtUUy6h1vXtckECtYPGhAw';

        echo 'JWE from partsadvisor : '.$inputJWEFromPartsAdvisorExample;

        // First step : Decrypt JWE and get JWS
        $jws = decryptJWE($inputJWEFromPartsAdvisorExample, $secretKey);

        // Second step : Verify and decode JWS to get payload
        echo 'Payload in JSON : ' . decodeJWS($jws, $signerKey);
    }
}